# NURISTalks

[![pipeline status](https://gitlab.com/nurlybekovnt/nuris-talks-2022/badges/master/pipeline.svg)](https://gitlab.com/nurlybekovnt/nuris-talks-2022/-/commits/master)
[![coverage report](https://gitlab.com/nurlybekovnt/nuris-talks-2022/badges/master/coverage.svg)](https://gitlab.com/nurlybekovnt/nuris-talks-2022/-/commits/master)
[![Latest Release](https://gitlab.com/nurlybekovnt/nuris-talks-2022/-/badges/release.svg)](https://gitlab.com/nurlybekovnt/nuris-talks-2022/-/releases)
