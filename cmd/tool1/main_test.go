package main

import "testing"

func TestSum(t *testing.T) {
	type testCase struct {
		a, b, expected int
	}
	testCases := []testCase{
		{1, 2, 3},
		{1, 3, 4},
	}
	for _, tc := range testCases {
		actual := Sum(tc.a, tc.b)
		if actual != tc.expected {
			t.Errorf("actual: %d, expected: %d", actual, tc.expected)
		}
	}
}
